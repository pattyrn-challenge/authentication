var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());

const whitelistUsers = [{
  email: 'test@example.com',
  password: '123456'
}];

app.post('/auth/authenticate', (req, res) => {
  const {
    email,
    password
  } = req.body;
  const authenticatedUser = whitelistUsers.find(user => email === user.email && password === user.password);
  if (authenticatedUser) {
    res.json({
      email: authenticatedUser.email
    });
  } else {
    res.sendStatus(401);
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
